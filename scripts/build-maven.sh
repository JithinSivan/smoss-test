#!/bin/bash
#
# Simple script that builds a maven project and renames the output to match entrypoint in Dockerfile.
#
#
#
#
chmod 777 ./target
set -x
set -e

#mvn -s $MAVEN_SETTINGS -B -X -e clean package -DskipTests 
mvn  -X -e clean package -DskipTests 
echo 'Install into the local Maven repository'
#mvn -s $MAVEN_SETTINGS jar:jar install:install
mvn jar:jar install:install



NAME=`mvn help:evaluate -Dexpression=project.name | grep "^[^\[]"`
VERSION=`mvn help:evaluate -Dexpression=project.version | grep "^[^\[]"`

#mv  target/${NAME}-${VERSION}.jar target/entrypoint.jar
mv target/SMOSS-1.0.jar target/entrypoint.jar